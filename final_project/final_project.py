#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import os
import reader


#Clase para ventana principal de visualizador de moléculas
class main_window():

    #Constructor de la clase
    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('final_project.glade')

        #Se utilizará la resolución máxima permitida por la pantalla del
        #usuario, para aprovechar las imagenes
        window = self.builder.get_object('main_window')
        window.maximize()
        window.connect('destroy', Gtk.main_quit)

        #Se crean los respectivo label para texto y la imagen respectiva
        self.folder_label = self.builder.get_object('file_route')
        self.detail_label = self.builder.get_object('molecule_details')
        self.molecule_image = self.builder.get_object('molecule_image')

        #Se crean los botones necesarios, incluido el botón "acerca de",
        #contenido en el menu bar, y se conectan son sus respectivos métodos
        self.select_btn = self.builder.get_object('open_btn')
        self.select_btn.connect('clicked', self.chooser)
        self.load_btn = self.builder.get_object('load_molecule')
        self.load_btn.connect('clicked', self.molecule_loader)
        self.edit_btn = self.builder.get_object('edit_molecule')
        self.edit_btn.connect('clicked', self.molecule_editor)
        self.about = self.builder.get_object('about_menu')
        self.about.connect('activate', self.about_press)

        #Se crea el treeview que contendrá las moleculas disponibles en el
        #directorio seleccionado por el usuario
        self.molecule_tree = self.builder.get_object('molecule_tree')
        self.molecules = Gtk.ListStore(str)
        self.molecule_tree.set_model(model=self.molecules)

        cell = Gtk.CellRendererText()
        tittle_column = Gtk.TreeViewColumn('Molécula', cell, text=0)
        self.molecule_tree.append_column(tittle_column)

        window.show_all()


    #Método para escoger directorio
    def chooser(self, btn=None):

        #Se instancia la ventana filechooser para escoger el directorio,
        #en el modo abrir archivo (mas adelante los detalles)
        file_chooser = chooser_dialog(Gtk.FileChooserAction.OPEN)
        response = file_chooser.chooser_window.run()

        #Si es escoge un directorio con al menos un archivo válido,
        #se obtiene el directorio y todos los archivos mol y mol2 de este
        if response == Gtk.ResponseType.OK:

            self.files, self.folder = file_chooser.get_files()

            #Se muestra el directorio selecionado en un label previamente
            #definido y se cargan en el treeview los nombres de los archivos
            #obtenidos previamente (en caso de haber seleccionado un
            #directorio previamente, se actualizarán los datos)
            self.folder_label.set_text('Ruta actual: %s' %(self.folder))
            self.clear_screen()
            self.show_data()


    #Método para ver y editar detalles de la molécula seleccionada
    def molecule_editor(self, btn=None):

        #Se obtiene la molécula seleccionada, en caso de ninguna, el método
        #no hara nada
        list, iter = self.molecule_tree.get_selection().get_selected()

        if list is None or iter is None:
            return

        #Se obtiene el nombre y se le suma la ruta del directorio, esto
        #permite obtener la ruta del archivo seleccionado y asi se podrá
        #abrir cualquier archivo en cualquier ruta, aunque no esten en el
        #mismo directorio que el programa
        name =  list.get_value(iter, 0)
        route = (self.folder + '/') + name

        #Se obtiene el formato del archivo seleccionado (para poder guardar
        #con el mismo formato en caso de edición)
        if name[-1:] == '2':
            format = '.mol2'

        else:
            format = '.mol'

        #Se instancia ventana(clase) de edición de archivos
        edit = edit_dialog(route)
        signal = edit.editor.run()

        #En caso de que se presione guardar en la ventana
        if signal == Gtk.ResponseType.OK:

            #Se llama al filechooser nuevamente pero en modo guardar
            save_route = chooser_dialog(Gtk.FileChooserAction.SAVE)
            response = save_route.chooser_window.run()

            #Se llama al método que permite obtener los nuevos datos ingresados
            data = edit.save_press()

            #En caso de presionar OK en el filechooser
            if response == Gtk.ResponseType.OK:

                #Se obtiene el nombre con el que se quiere guardar el archivo
                save_name = save_route.chooser_window.get_filename()

                #El nombre se setea en blanco para poder comprobar si el
                #nombre ingresado previamente es válido
                save_route.chooser_window.set_current_name('')

                #Se obtienen todos los nombres en el directorio seleccionado,
                #incluido el que se esta ingresando, por eso se debe cambiar
                #el nombre por un espacio vacío
                if save_name in save_route.chooser_window.get_filenames():

                    #Si el nombre esta repetido, se abre una ventana de error
                    error_dialog()

                #Si se ingresa un nombre sin formato, este aún puede ser valido
                elif save_name[-(len(format)):] != format:

                    valid = True
                    for i in save_name:
                        if i == '.' or i == ' ':
                            #Pero si se entregó con otro formato, este no es
                            #valido y se abre la ventana de error
                            error_dialog()
                            valid = False

                    if valid:

                        #En caso de ser valido, se le agrega el formato antes
                        #obtenido y se crea el archivo en cuestión
                        save_name = save_name + format

                        with open(save_name, 'w') as file:
                            file.write(data)
                        file.close()

                        #Se vuelven a obtener archivos del directorio, con el
                        #propósito de actualizar datos
                        self.files, aux = save_route.get_files()
                        self.files.append([save_name.replace(self.folder,
                                                             '')[1:]])
                        self.folder_label.set_text('Ruta actual: %s'
                                                   %(self.folder))
                        save_route.chooser_window.destroy()

                #En caso de ser un nombre válido y con formato correcto
                #ingresado manualmente, se crea y guarda el archivo
                else:

                    with open(save_name, 'w') as file:
                        file.write(data)
                    file.close()

                    #También se actualizan los datos
                    self.files, aux = save_route.get_files()
                    self.files.append([save_name.replace(self.folder, '')[1:]])
                    self.folder_label.set_text('Ruta actual: %s' %(self.folder))
                    save_route.chooser_window.destroy()

        #Se actualizan los datos en pantalla
        self.clear_screen()
        self.show_data()

    #Método para limpiar datos en pantalla
    def clear_screen(self):

        #Solo se limpian datos si es que estos existen en pantalla
        if len(self.molecules) != 0:
            #Este metodo remueve selectivamente (uno por uno y de forma
            #especifica) todos los elementos de la lista que usa como
            #modelo el gtk.treeview
            for i in range(len(self.molecules)):
                iterator = self.molecules.get_iter(0)
                self.molecules.remove(iterator)


    #Método para cargar datos en treeview
    def show_data(self):

        #Se agregan los datos obtenidos por el filechooser en la lista que
        #utiliza como modelo el treeview
        for molecule in self.files:
            self.molecules.append(molecule)


    #Método para cargar molécula seleccionada
    def molecule_loader(self, btn=None):

        #Se obtiene molécula seleccionada si es que existe una seleccionada,
        #sino, no se hace nada
        list, iter = self.molecule_tree.get_selection().get_selected()

        if list is None or iter is None:
            return

        #Se obtiene la ruta del archivo seleccionado y se llama al módulo que
        #leerá los datos de la molécula
        name =  list.get_value(iter, 0)
        route = (self.folder + '/') + name
        elements, percentages, format = reader.element_reader(route)

        #También se carga un diccionario JSON que contiene los símbolos de
        #los elementos químicos con sus nombre completos (hecho a mano, con
        #mucha paciencia)
        with open('elements.json') as file:
            full_names = json.load(file)
        file.close()

        #Se llenan datos para agregar en el label que mostrará los detalles de
        #la molecula, empezando por el nombre del archivo y su formato
        head = ('Nombre: %s \n'
                'Formato: %s \n' %(name, format))

        details = 'Detalles \n'

        #Después se agregan los datos de cuantos atomos se encontraron y su
        #porcentaje en cantidad, no en masa, ademas se cambia el símbolo por
        #el nombre completo con ayuda del diccionario antes mencionado
        for i in range(len(elements)):
            elem = full_names[elements[i]]
            details = details + ('\n%s: %d porciento' %(elem, percentages[i]))

        #Se muestran los datos creados en el label correspondiente
        self.detail_label.set_text('%s \n'
                                   '%s' %(head, details))

        #Se obtiene el nombre de la imagen del archivo (hipotetico en caso de
        #no existir)
        img_name = name[:-(len(format) + 1)] + '.png'
        img_route = (self.folder + '/') + img_name

        #Se trata de abrir la imagen y mostrarla en pantalla, solo si esta
        #existe en la ruta de la molécula
        try:
            open(img_route) #open solo para comprobar existencia de imagen
            self.molecule_image.set_from_file(img_route)

        #Pero si esta no existe, se crea con ayuda de pymol y se muestra en
        #pantalla
        except IOError:

            os.system('pymol %s -x -g %s -c -Q' %(route, img_route))
            self.molecule_image.set_from_file(img_route)


    #Método que abre ventana de aboutdialog
    def about_press(self, btn=None):

        about_dialog()


#Clase para ventana de filechooser
class chooser_dialog():

    #Constructor recibe el modo en el que se abrirá la ventana
    def __init__(self, mode):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('final_project.glade')

        #Se configura ventana con el modo que recibió previamente
        self.chooser_window = self.builder.get_object('chooser_window')
        self.chooser_window.set_default_size(1200, 900)
        self.chooser_window.set_action(mode)
        self.chooser_window.show_all()

        #Se crea y agrega un filtro que hará que solo se puedan visualizar
        #carpetas y archivos en formato mol y mol2
        mol_filter = Gtk.FileFilter()
        mol_filter.set_name('Valid Molecules (.mol, .mol2)')
        mol_filter.add_pattern('*.mol')
        mol_filter.add_pattern('*.mol2')
        self.chooser_window.add_filter(mol_filter)

        #Se crean dos botones, solo que uno emite una señal y el otro esta
        #conectado a un método para cerrar la ventana
        self.ok_btn = self.builder.get_object('chooser_ok')
        self.cancel_btn = self.builder.get_object('chooser_cancel')
        self.cancel_btn.connect('clicked', self.cancel_pressed)

    #Método para obtener nombres de archivos y su ruta
    def get_files(self, btn=None):

        #Se pone en modo abrir siempre, debido a que también se llama a esta
        #función para actualizar datos
        self.chooser_window.set_action(Gtk.FileChooserAction.OPEN)

        #Se permite seleccionar mas de un archivo a la vez y se seleccionan
        #todos los archivos mol y mol2 de la ruta seleccionada
        self.chooser_window.set_select_multiple(True)
        self.chooser_window.select_all()

        #Se obtienen los nombres completos (incluyendo su ruta) y se obtiene
        #el directorio seleccionado
        files = self.chooser_window.get_filenames()
        folder = self.chooser_window.get_current_folder()

        #Se crea un arreglo que contendrá nombres de archivos en el directorio
        file_names = []

        #Se agregan los nombres sin su ruta, para hacer mas facil su lectura
        for i in files:
            file_names.append([i.replace(folder, '')[1:]])

        self.chooser_window.destroy()

        #Se retornan los nombres y el directorio obtenido
        return file_names, folder

    #Método que cierra la ventana en caso de apretar cancelar
    def cancel_pressed(self, btn=None):
        self.chooser_window.destroy()


#Clase para ventana de edición de molécula
class edit_dialog():

    #Constructor que recibe la ruta de la molécula seleccionada
    def __init__(self, name):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('final_project.glade')

        self.editor = self.builder.get_object('edition_dialog')
        self.editor.set_title('Molecule details')
        self.editor.show_all()

        #Se crean los botones guardar y cancelar, y se conecta cancelar con su
        #método correspondiente, guardar por su parte, emite una señal
        self.save_btn = self.builder.get_object('save_changes')
        self.cancel_btn = self.builder.get_object('cancel_changes')
        self.cancel_btn.connect('clicked', self.cancel_press)

        #Se carga el textview y su buffer(el texto que muestra)
        self.molecule_data = self.builder.get_object('molecule_text')
        self.molecule_text = self.molecule_data.get_buffer()

        #Se abre la molécula y se vuelca su contenido en el buffer para ser
        #visualizado y editado
        with open(name) as file:
            data = file.read()
            self.molecule_text.set_text(data)

        file.close()

    #Método en caso de querer guardar datos
    def save_press(self, btn=None):

        #Debido a como funciona la obtención de datos del buffer, se obtiene
        #la primera y la ultima linea, y despues se lee desde la primera a la
        #ultima, asi se obtienen los datos ingresados y se retornan como
        #contenidos en una variable
        start = self.molecule_text.get_start_iter()
        end = self.molecule_text.get_end_iter()
        new_data = self.molecule_text.get_text(start, end, False)
        self.editor.destroy()

        return new_data

    #Método que cierra ventana en caso de presionar cancelar
    def cancel_press(self, btn=None):
        self.editor.destroy()


#Clase para mensaje de error
class error_dialog():

    #No se requiere cometario adicional, solo ventana de dialogo con un
    #mensaje de nombre no valido y un botón para cerrar mensaje
    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('final_project.glade')

        self.error = self.builder.get_object('error_dialog')
        self.error.set_markup('Nombre no valido')
        self.error.format_secondary_text('Por favor, ingrese un nombre valido,'
                                         'o que no este en uso')

        self.close_btn = self.builder.get_object('close_dialog')
        self.close_btn.connect('clicked', self.close_press)
        self.error.show_all()

    #Método que cierra ventana
    def close_press(self, btn=None):
        self.error.destroy()


#Clase para aboutdialog (acerca de...)
class about_dialog():

    #Constructor
    def __init__(self):

        #Se carga ventana con datos sobre creador, profesor y programa,
        #adicionalmente con una foto de la hija del desarrollador
        self.builder = Gtk.Builder()
        self.builder.add_from_file('final_project.glade')

        self.dialog = self.builder.get_object('about_dialog')
        self.dialog.show_all()

        self.dialog_ok = self.builder.get_object('about_ok')
        self.dialog_ok.connect('clicked', self.ok_press)

    #Método que cierra ventana en caso de presionar el único boton presente
    def ok_press(self, btn=None):
        self.dialog.destroy()


#Función Main
if __name__ == '__main__':

    main_window()
    Gtk.main()


#Funcion para leer archivo mol o mol2
def element_reader(name):

    #Se abre la ruta enviada desde el modulo principal
    #ademas se crea arreglo que contendrá el total de atomos encontrados
    data = open('%s' %(name))
    read = False
    atoms = []

    #En caso de ser un archivo mol2, se empieza a leer desde el patrón
    #entregado y se termina de leer al encontrar el patrón entregado
    if name[-4:] == 'mol2':

        type = 'mol2'
        for line in data:
            #Este patrón indica que a continuación estan los datos de cada
            #átomo individual
            if len(line)>12 and line[:13] == '@<TRIPOS>ATOM':
                read = True

            #Este indica que a continuación vienen los datos sobre enlaces,
            #por lo que se deja de leer el archivo
            elif len(line)>12 and line[:13] == '@<TRIPOS>BOND':
                break

            #El símbolo atómico, siempre se situa en la misma posicion
            #en el archivo mol2
            elif read:
                atoms.append(line[8])

    #En caso de ser archivo mol, se cambia el patrón de busqueda para leer
    else:

        type = 'mol'
        for line in data:

            #mol es algo mas implícito para sus datos, por lo que se busca un
            #patrón menos explícito para saber donde leer y donde no
            if read and line[:3] != '   ':
                break

            elif len(line)>3 and line[:3] == '   ':
                read = True

            if read:
                atoms.append(line[31])


    #Se obtiene el total de átomos y se crea un arreglo para los elementos
    #encontrados y uno para el porcentaje de cada uno
    total = len(atoms)
    elements_found = []
    percentages = []

    #El arreglo de elementos solo tendrá una vez cada elemento encontrado
    for element in atoms:
        if element not in elements_found:
            elements_found.append(element)

    #Se busca cuantas veces se repite el elemento en el arreglo inicial, y
    #con ayuda de la función count (cuenta cuantas veces se repite un
    #patrón en un arreglo o variable), finalmente se calcula el porcentaje
    for element in elements_found:
        percentages.append(round(((atoms.count(element))/total)*100, 0))

    #Se retornan los elementos encontrados, el porcentaje de cada uno
    #y el tipo de archivo que se leyó
    return elements_found, percentages, type

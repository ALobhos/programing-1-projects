import json
import turtle
from random import shuffle, choice
import path_checker
screen = turtle.Screen()

#This function will read the actual register backwards
#moving the player when it reads
def backtrack(player, register):

    backroad = len(register) - 1
    player.pendown()
    while backroad >= 0:
        player.goto(register[backroad])
        backroad -= 1
    player.penup()

#This will store the visited rooms in a array
def already_visited(intention, visited):

    value = False
    for i in visited:
        if i == intention:
            value = True
            break
    return value

#This function uses the same method used to create the maze
#basically runs in a random direction, trying to visit all
#of the maze, but this will stop when finds the goal
def solve(player, start, goal, maze):

    visited = []
    memory = []
    def walk(start, goal):

        x = start[0]
        y = start[1]
        move = [(x+25, y), (x-25, y), (x, y+25), (x, y-25)]
        shuffle(move)
        if start != goal:
            for intention in move:
                if already_visited(intention, visited):
                    pass
                elif path_checker.path_check(intention, maze):
                    visited.append(intention)
                    memory.append(intention)
                    if goal == intention:
                        break
                    else:
                        walk(intention, goal)
    walk(start, goal)

    for move in memory:
        player.goto(move)
        if player.position() == goal: break


#Takes control of the player movement, reading a teaching or finding
#a way to the goal, also this will start moving from the actual 
#position of the player, no matter where it is

def take_control(known_path, path, player, start, goal, maze):

    if known_path:
        ready = False
        for move in path:
            if ready:
                player.goto(move)
            elif start == move:
                ready = True

    else:
        option = screen.numinput("New maze?",
                           "I don't know this maze...\n"
                          "Do you want me to try to solve it?\n"
                           "(1)Yes / (2)No")

        if option == 1:
            rogue = screen.numinput("Please",
                              "Please, give me a teaching\n"
                              "solve it and teach me please\n"
                              "(1)Ok / (2)No")

            if rogue == 2:
                solve(player, start, goal, maze)
            else:
                screen.textinput("Thanks",
                                 "Thanks, now go and try to solve the maze"
                                 "when you make it"
                                 "I will learn how to solve it")

        else:
            screen.textinput("NMV", "Then go and solve it")


#Pathfinder is the main funcion of Pepe (Pepe is the name of IA)
#Pepe can give you motivational phrases or lead you to the goal,
#or start, or even tell you how you came here
def pathfinder(player, seed, goal, maze, register):

    option = screen.numinput("Pepe", "What do you need?:\n"
                             "(1)Find the exit\n"
                             "(2)Find the entrance\n"
                             "(3)How did I get here?\n"
                             "(4)Motivational phrase\n"
                             "(5)Nothing", default=5, minval=1, maxval=5)

    start = []
    for i in player.pos():
        start.append(i)

    with open("known_paths.json") as file:
        data = json.load(file)
        if option == 1:
            if seed in data:
                for path in data[seed][0]:
                    if len(data[seed][0][path]) > 25:
                        for coordinates in data[seed][0][path]:

                            if coordinates == start:
                                take_control(True, data[seed][0][path],
                                             player, start, goal, maze)
                                break
            else:
                take_control(False, data, player, start, goal, maze)
            screen.textinput("Pepe", "Done")
        elif option == 2:
            take_control(False, data, player, start, (-300, 300), maze)
            screen.textinput("Pepe", "Done")

        elif option == 3:
            backtrack(player, register)
            screen.textinput("Pepe", "Done")

        elif option == 4:
            phrases = [("c'mon, do it"), ("You can do it"),
                       ("Look, i'm only an algorithm, not a man, just do it"),
                       ("DO IT, NOW"), ("Man, it's easy"),
                       ("I have my cat as background, so you can solve this"),
                       ("do dis meis, yu can du it")]

            screen.textinput("Phrase", choice(phrases))

        else:
            screen.textinput("Hi", "Hi, my name is pepe, "
                             "If you don't need anything, then don't call me."
                             "\n"
                             "Later...")

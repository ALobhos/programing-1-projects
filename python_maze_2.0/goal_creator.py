import turtle
from random import randrange

def create(valid_exit):

    orientation = randrange(2)

    if orientation == 1:
        exit_position = (300, -300 + (25*randrange(25)))
    else:
        exit_position = (-300 + (25*randrange(25)), -300)

    valid = False
    for i in range(311):
        if exit_position == valid_exit[i].position():
            valid = True
            break

    if valid:
        exit = turtle.Turtle()
        exit.speed('fastest')
        exit.penup()
        exit.shapesize(1, 1)
        exit.hideturtle()
        exit.shape('circle')
        exit.color('blue')
        exit.goto(exit_position)
        exit.showturtle()
        return exit_position

    else:
        exit_position = create(valid_exit)

    #print("retornada: ", exit_position)
    return exit_position


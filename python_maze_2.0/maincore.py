import turtle
import json
from random import randrange

screen = turtle.Screen()

def core():
    screen.clear()
    screen.setup(700, 700)

    j = 0
    fila = 0
    camino = []
    registro = [(-300, 300)]

#Menus
#One opens whenever the player press "Esc", this is the escape menu
#The other only opens when the player reaches the goal
    def escape_menu():
        option = screen.numinput('Menu',
                                 '(1) Enable IA\n'
                                 '(2) Main Menu\n'
                                 '(3) Exit Python Maze',
                                 default=None, minval=1, maxval=3)

#for details of IA seed IA module
        if option == 1:

            import IA
            IA.pathfinder(player, seedcode, true_exit, camino, registro)
            screen.listen()
            check()

        elif option == 2:
            main_menu()

        else:
            turtle.bye()


    def remake(register):

        player.ht()
        player.goto(-300, 300) #always add the starting position
        player.st()
        player.pendown()

        for movements in register:
            player.goto(movements)


    def victory_menu():
        screen.title("Victory!")
        option = screen.numinput("Victory", "Victory, you or Pepe made it, "
                                 "What do you want to do?\n"
                                 "(1) Review game(Your moves, not Pepe)\n"
                                 "(2) Main Menu\n"
                                 "(3) Exit Python Maze 1.1",
                                 default=3, minval=1, maxval=3)

        if option == 1:
            remake(registro)
            victory_menu()
        elif option == 2:
            main_menu()
        else:
            turtle.bye()

#Reading of json data
#Thiis part will read the json files in search for the seedcodes
#I used seedcodes in numbers, because for me it's more easy to
#convert characters to numbers

    import maze_generator
    import goal_creator

    seed = screen.textinput("Seed", "Insert seed for a random maze")
    if seed == None or seed == "":
        seed = randrange(100000000)
        seedcode = str(seed)
    else:
        seedcode = ""
        for i in seed:
            seedcode = seedcode + str(ord(i))


    with open("mazes.json") as file:
        maze = json.load(file)

        if seedcode in maze:
            matriz = maze[seedcode][0]["matrix"]
            exit = maze[seedcode][0]["exit"]
            goal = turtle.Turtle()
            goal.shape('circle')
            goal.color('blue')
            goal.penup()

        else:
            matriz = maze_generator.walk(12, 12)

#save data functions
#it recovers the data from a json, then add the new data
#finally clears the json and dump the new data in it

    def save_seed(matriz, true_exit):
        maze[seedcode] = []
        maze[seedcode].append({"matrix": matriz,
                               "exit": true_exit})
        with open("mazes.json", "w") as file:
            json.dump(maze, file, indent=4)

    def save_path(path):

        with open("known_paths.json") as file:
            data = json.load(file)

            if seedcode not in data:
                data[seedcode] = []
                paths = len(data[seedcode])
                data[seedcode].append({"path"+str(paths): path})
            with open("known_paths.json", 'w') as file:
                json.dump(data, file, indent=4)

#Loading the maze setup, from random or already known
#This part reads an invisible matrix, and creates turtles
#for every 1 in the matrix, using cicles will determine
#the position of the turtles

    screen.title("Loading...")
    for i in matriz:
        columna = 0
        for k in i:
            if k == 1:
                camino.append(0)
                camino[j] = turtle.Turtle()
                camino[j].shape('square')
                camino[j].penup()
                camino[j].color('white')
                camino[j].shapesize(2.15, 2.15)
                camino[j].speed('fastest')
                camino[j].goto(-300+(columna*25), 300-(fila*25))
                j+=1
            columna+=1
        fila+=1
    screen.bgcolor('black')

    if seedcode not in maze:
        true_exit = goal_creator.create(camino)
        save_seed(matriz, true_exit)

    else:
        goal.goto(exit)
        true_exit = goal.position()

#creating a player

    player = turtle.Turtle()
    player.shape('circle')
    player.penup()
    player.color('red')
    player.speed('slowest')
    player.goto(-300, 300)
    screen.title("Python Maze 1.1")

#Victory and valid path checks
#Just checks, for details on path_checker see the module itself
    def check():
        if player.position() == true_exit:
            save_path(registro)
            victory_menu()

    def valid_path(movement):

        import path_checker
        path = path_checker.path_check(movement, camino)

        return path

#Movement funcions
#This funcion is always focused on the keyboard
#If an arrow is pressed, moves the player using a dictionary
#Also can open the escape menu by pressing "Esc"
    def move_r(): move('Right')
    def move_l(): move('Left')
    def move_u(): move('Up')
    def move_d(): move('Down')

    def move(pressed):

        x = player.xcor()
        y = player.ycor()
        movement = {'Right': (x+25, y), 'Left': (x-25, y),
                    'Up': (x, y+25), 'Down': (x, y-25)}

        if valid_path(movement[pressed]):
            player.goto(movement[pressed])
            registro.append(movement[pressed])
            check()

    screen.onkey(move_r, 'Right')
    screen.onkey(move_l, 'Left')
    screen.onkey(move_u, 'Up')
    screen.onkey(move_d, 'Down')
    screen.onkey(escape_menu, 'Escape')
    screen.listen()


#On title menu we have a pointer (literally a turtle)
#The turtle can be moved with the arrows in only 3 positions

def main_menu():

    screen.clear()
    screen.setup(500, 500)
    screen.bgpic("title.gif")
    screen.title("Python Maze 1.1")

    pointer = turtle.Turtle()
    pointer.penup()
    pointer.ht()
    pointer.shape("turtle")
    pointer.color("white")
    pointer.goto(-150, -100)
    pointer.st()

    function = {(-150, -100): 1,
                (-150, -150): 2,
                (-150, -200): 3}

#These coordinates match the options in the background image
#Yes, it's only a image and a turtle

    def go_up():
        if pointer.position() != (-150, -100):
            pointer.goto(pointer.position() + (0, 50))

    def go_down():
        if pointer.position() != (-150, -200):
            pointer.goto(pointer.position() - (0, 50))

    def give_option():
        use = function[pointer.position()]

        if use == 1:
            core()
        elif use == 2:
            screen.textinput("Credits",
                             "Python Maze 1.1\n"
                             "Made by Arturo Lobos\n"
                             "Professor: Fabio Durán\n"
                             "Escuela de Ingeniería Civil en Bioinformática")
            main_menu()
        else:
            turtle.bye()


    screen.listen()
    screen.onkey(go_up, 'Up')
    screen.onkey(go_down, 'Down')
    screen.onkey(give_option, 'Return') #Return is also known as "enter"
    screen.mainloop()

main_menu()

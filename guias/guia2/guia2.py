import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json

#Funcion creada para obtener el elemento del combobox
def get_from_combo(combo):

    #Primero se obtiene el iterador activo del combobox (debido a que )
    combo_aux = combo.get_active_iter()
    #Si el combobox tiene algo seleccionado, se obtiene el valor del
    #iterador en el modelo (la lista) que esta utilizando el combobox,
    #(entiendase por valor: el nombre)
    if combo_aux is not None:
        model = combo.get_model()
        name = model[combo_aux][0]
    else:
        #Si no hay nada seleccionado, se devuelve el nombre como None
        name = None

    #Se retorna el nombre obtenido
    return name

def checked(new_name, new_compos, new_type, data, old_name):

    #Se inician dos variables que seran la validez y el tipo de error
    check = True
    error = None

    #Entra en funcion la variable old_name, que permite que cuando se este
    #editando algun compuesto, la funcion check haga un chequeo adicional
    #que permite que el compuesto pueda ser guardado con el mismo nombre,
    #pero no con el nombre de otro compuesto
    if len(data) != 0:
        for i in data:
            for saved in data[i]:
                if saved[0] != old_name:
                    if saved[0] == new_name:
                        #En caso de no ser un nombre valido, se marca como
                        #no valido y se dice que el error es nombre repetido
                        check = False
                        error = 'repetido'

    #Si el nombre esta en blanco, sera no valido y el error sera
    #un nombre en blanco
    if new_name == '':
        error = 'Nombre'
        check = False

    #Si la composicion esta en blanco, no sera valida y el error
    #sera composicion en blanco
    elif new_compos == '':
        error = 'Composicion'
        check = False

    #Si el tipo de compuesto no se ha selecionado, se invalidan los datos
    #y el error sera tipo no especificado
    elif new_type is None:
        error = 'Tipo'
        check = False

    #Se retorna una validacion y un error en caso de existir, en caso de
    #no haber error, la variable error es None
    return check, error


#Funcion para obtener datos del archivo json
def get_data():

    try:
        #Se carga el archivo json y se almacena en data
        with open('compound_register.json') as file:
            data = json.load(file)
        file.close()

    #En caso de no existir el archivo json, se crea la variable data
    except IOError:
        data = {}

    #Se retornan los datos obtenidos o creados
    return data


#Funcion para guardar datos en archivo json
def save_data(data):

    with open('compound_register.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()


#Se crea una ventana principal que contiene 3 botones, un gtk.tree
#y un expander que oculta dos gtk.entry y un gtk.combobox
class main_window():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia2.glade')

        #Se utiliza el comando maximize para que la ventana utilice la maxima
        #resolucion posible al abrirse
        self.window = self.builder.get_object('main_window')
        self.window.maximize()
        self.window.connect('destroy', Gtk.main_quit)

        #Se cargan los botones para añadir, borrar y editar, y se conectan con
        #sus respectivos metodos
        self.add_btn = self.builder.get_object('add_compound')
        self.add_btn.connect('clicked', self.add_pressed)

        self.edit_btn = self.builder.get_object('edit_compound')
        self.edit_btn.connect('clicked', self.edit_pressed)

        self.del_btn = self.builder.get_object('del_compound')
        self.del_btn.connect('clicked', self.del_pressed)

        #Ademas se carga el expander para su utilizacion puntual
        #en algunos metodos
        self.expander = self.builder.get_object('expander')

        #Se crea un liststore que almacenara los datos que se visualizaran
        #en el treeview, ademas se crea la cantidad necesaria de columnas
        self.list = Gtk.ListStore(str, str, str)
        self.compound_tree = self.builder.get_object('main_tree')
        self.compound_tree.set_model(model=self.list)

        cell = Gtk.CellRendererText()

        titles = ('Nombre', 'Composición', 'Tipo')

        for i in range(3):
            column = Gtk.TreeViewColumn(titles[i], cell, text=i)
            self.compound_tree.append_column(column)

        #Se cargan los entry y el combobox que serviran para añadir o editar
        #algun compuesto segun se requiera
        self.name_entry = self.builder.get_object('name')
        self.composition_entry = self.builder.get_object('composition')
        self.type_combo = self.builder.get_object('type')

        #Se llama al metodo para cargar los datos en el liststore
        self.show_data()
        self.window.show_all()

    #Metodo para añadir compuestos
    def add_pressed(self, btn=None):

        #Se carga cada entry y los datos que aporte el combobox
        #usando la funcion get_from_combo
        name = self.name_entry.get_text()
        composition = self.composition_entry.get_text()
        type = get_from_combo(self.type_combo)

        #se obtienen los datos del archivo json
        data = get_data()

        #Se llama a la funcion checked y se le envian los datos ingresados
        #notese el None al final de los datos enviados, esto se debe a que
        #esta funcion se comparte con el metodo para editar datos.
        #Los valores obtenidos son un boolean que determina la validez y
        #una variable auxiliar en caso de existir un error.
        #(detalles sobre el error en la funcion checked)
        valid, aux = checked(name, composition, type, data, None)

        if valid:

            #Si los datos son validos se guardan en los datos obtenidos del
            #archivo json, y si el archivo esta vacio, se crea y llena
            if type not in data:
                data[type] = []

            #Se forma un arreglo con dos dato y se añade a los datos
            add = [name, composition]

            data[type].append(add)

            #Los datos son guardados en el json usando la funcion save_data
            #ademas se limpian todas las entradas que se ven en pantalla
            save_data(data)
            self.name_entry.set_text('')
            self.composition_entry.set_text('')
            self.type_combo.set_active(-1)

            #Se llama al metodo que limpia los datos mostrados y nuevamente
            #se llama a la funcion que muestra los datos, esto para que se
            #actualicen los valores mostrados en pantalla
            self.clear_screen()
            self.show_data()

        else:
            #Si los datos no fueran validos, se expande el gtk.expander
            #(solo en caso de no estar expandido), esto por si acaso el
            #usuario no hubiera visto el expander
            if not self.expander.get_expanded():
                self.expander.set_expanded(True)

            #Se muestra una ventana de error en la que se especifica el
            #tipo de error encontrado
            error_dialog(aux)


    #Metodo para limpiar datos en pantalla
    def clear_screen(self):

        #Solo se limpian datos si es que estos existen en pantalla
        if len(self.list) != 0:
            #Este metodo remueve selectivamente (uno por uno y de forma
            #especifica) todos los elementos de la lista que usa como
            #modelo el gtk.treeview
            for i in range(len(self.list)):
                iterator = self.list.get_iter(0)
                self.list.remove(iterator)

    #Metodo para cargar y mostrar datos almacenados
    def show_data(self):

        #Se obtienen los datos del json mediante la funcion get_data
        data = get_data()

        #Se lee cada elemento del json y se crea una tupla de 3 elementos
        #que contiene el nombre, la composicion y el tipo
        for type in data:
            for elements in data[type]:
                elements.append(type)
                self.list.append(elements)

    #Metodo para eliminar compuestos
    def del_pressed(self, btn=None):

        #Se obtiene el elemento de la lista (list) y el iterador (la fila)
        #actual de la zona selecciona por el usuario
        list, iter = self.compound_tree.get_selection().get_selected()

        #En caso de que el usuario no haya seleccionado nada, el metodo
        #no hara nada
        if list is None or iter is None:
            return

        #Si hay algo seleccionado, se obtienen los datos del archivo json
        data = get_data()
        #Se instancia una ventana de dialogo a la que se le envia solo el
        #valor del primero elemento de la fila (solo se le envia el nombre)
        confirmation = delete_dialog(list.get_value(iter, 0))
        response = confirmation.del_window.run()

        #En caso de que se presione aceptar en la ventana de confirmacion
        if response == Gtk.ResponseType.OK:

            #Se busca el elemento que contenga el nombre elegido, cuando
            #se encuentre, se elimina el elemento completo
            #(nombre y composicion)
            for type in data:
                for name in data[type]:
                    if name[0] == list.get_value(iter, 0):
                        data[type].remove(name)

            #Se guarda la nueva lista en el archivo json, y se actualiza la
            #pantalla limpiando y mostrando los datos nuevamente
            save_data(data)
            self.clear_screen()
            self.show_data()

    #Metodo para editar datos
    def edit_pressed(self, btn=None):

        #Nuevamente se obtiene la seleccion hecha por el usuario
        list, iter = self.compound_tree.get_selection().get_selected()

        #En caso de no haber seleccionado nada, el metodo tampoco hara nada
        if list is None or iter is None:
            return

        #Se obtienen los datos del archivo json
        data = get_data()

        #Con la seleccion obtenida, se obtienen los valores de cada celda
        #de la fila seleccionada y se colocan estos valores en los entry
        #utilizados para añadir elementos
        self.name_entry.set_text(self.list.get_value(iter, 0))
        self.composition_entry.set_text(self.list.get_value(iter, 1))
        #self.change = iter
        self.old_name = self.name_entry.get_text()

        #Debido a la naturaleza del combobox, se utiliza otro procedimiento
        #similar para colocar su valor en la zona de edicion
        if self.list.get_value(iter, 2) == 'Orgánico':
            self.type_combo.set_active(0)

        else:
            self.type_combo.set_active(1)

        #Se crea de forma auxiliar, dos botones (guardar y cancelar), estos
        #se agregan debajo de los entry que se utilizaran
        self.confirm = Gtk.Button(label='Guardar')
        self.cancel = Gtk.Button(label='Cancelar')
        #Se obtiene el grid en el que estan contenidos los entry con sus
        #label, solo para poder agregarle elementos
        self.aux_grid = self.builder.get_object('add_grid')
        self.aux_grid.attach(self.confirm, 1, 3, 1, 1)
        self.aux_grid.attach(self.cancel, 0, 3, 1, 1)

        #En caso de que el gtk.expander no este desplegado, se expande
        if not self.expander.get_expanded():
            self.expander.set_expanded(True)
        self.window.show_all()

        #Se conectan los botones auxiliares con dos metodos propios de la
        #edicion de datos
        self.confirm.connect('clicked', self.save_changes)
        self.cancel.connect('clicked', self.cancel_edit)

    #Metodo propio de edicion que guarda los cambios, en caso de presionar
    #el boton guardar
    def save_changes(self, btn=None):

        #Se obtienen los datos de los entry y el combo
        new_name = self.name_entry.get_text()
        new_composition = self.composition_entry.get_text()
        new_type = get_from_combo(self.type_combo)

        #Se cargan los datos del archivo json
        data = get_data()

        #Nuevamente se llama a la funcion checked, pero ahora si se manda
        #un ultimo valor: El nombre original del compuesto que se quiere
        #modificar
        valid, aux = checked(new_name, new_composition,
                             new_type, data, self.old_name)

        #En caso de ser validos los datos, se remueve la version anterior
        #del compuesto y se guarda la nueva, que puede incluso tener el
        #mismo nombre anterior
        if valid:

            for type in data:
                for elements in data[type]:
                    if elements[0] == self.old_name:
                        data[type].remove(elements)

            #Se añade todo a la lista
            add = [new_name, new_composition]

            data[new_type].append(add)
            #Se guardan los datos en el archivo json
            save_data(data)

            #Se vuelven a limpiar los valores de los entry y el combobox
            self.name_entry.set_text('')
            self.composition_entry.set_text('')
            self.type_combo.set_active(-1)
            #Se actualiza la informacion en pantalla
            self.clear_screen()
            self.show_data()

            #Se destruyen los botones de guardar y cancelar, debido a que
            #estos son propios de editar datos y solo se activan cuando
            #se quiere cambiar algo
            self.confirm.destroy()
            self.cancel.destroy()

        else:
            #En caso de no ingresar datos validos (como repetir otro nombre
            #o dejar algun valor en blanco), se expande el gtk.expander en
            #caso de no estar expandido y se levanta una ventana de error
            #que especifica el tipo de error
            if not self.expander.get_expanded():
                self.expander.set_expanded(True)
            error_dialog(aux)

    #Metodo para cancelar edicion si se apreta el boton cancelar
    def cancel_edit(self, btn=None):

        #Se destruyen ambos botones (cancelar y guardar) y se limpian los
        #valores de los entry y el combobox
        self.cancel.destroy()
        self.confirm.destroy()
        self.name_entry.set_text('')
        self.composition_entry.set_text('')
        self.type_combo.set_active(-1)


#Ventana de error en caso de no ingresar datos validos
class error_dialog():

    #constructor de la ventana, este ademas recibe el tipo de error que
    #entrego la funcion checker
    def __init__(self, error):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia2.glade')
        self.error = error

        self.error_window = self.builder.get_object('error_window')
        self.error_window.show_all()

        #Solo se tiene un boton, que cierra la ventana
        self.error_btn = self.builder.get_object('error_ok')
        self.error_btn.connect('clicked', self.close_dialog)

        #En caso de que el error sea un nombre repetido se muestra un
        #mensaje explicando que ya existe un compuesto con ese nombre
        if error == 'repetido':
            self.error_window.set_markup('El nombre ingresado ya se'
                                             ' encuentra registrado')

        #En caso de dejar un campo en blanco, se pide que se complete la
        #seccion correspondiente
        else:
            self.error_window.set_markup('Por favor, complete la sección: '
                                         '%s' %(self.error))

    #Metodo que cierra la ventana de error
    def close_dialog(self, btn=None):
        self.error_window.destroy()


#Ventana que confirma querer borrar algun elemento
class delete_dialog():

    #Constructor de la ventana que ademas recibe el nombre del compuesto
    #que se desea eliminar
    def __init__(self, delete):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia2.glade')

        self.del_window = self.builder.get_object('del_window')
        self.del_window.show_all()

        #Se crean dos botones, uno para confirmar la delecion del elemento
        #y otro para cancelar
        self.del_ok = self.builder.get_object('del_ok')
        self.del_ok.connect('clicked', self.close_dialog)
        self.del_cancel = self.builder.get_object('del_cancel')
        self.del_cancel.connect('clicked', self.close_dialog)

        #Se configura la ventana para mostrar el mensaje de confirmacion
        #preguntando si de verdad desea elimiar tal elemento
        self.del_window.set_markup('¿Seguro que desea eliminar %s de la lista?'
                                   %(delete))

    #En caso de presionar cancelar se cierra la ventana y no se borra el
    #compuesto seleccionado
    def close_dialog(self, btn=None):
        self.del_window.destroy()

# funcion main
if __name__ == '__main__':

    main_window()
    Gtk.main()

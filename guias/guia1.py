import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class main_window():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia1.glade')

        window = self.builder.get_object('main_window')
        window.set_default_size(800, 600)
        window.connect('destroy', Gtk.main_quit)
        window.show_all()

        self.text1 = self.builder.get_object('text1')
        self.text1.connect('activate', self.sum_characters)
        self.text2 = self.builder.get_object('text2')
        self.text2.connect('activate', self.sum_characters)
        self.suma = self.builder.get_object('result')
        self.result = self.builder.get_object('sum_button')
        self.result.connect('clicked', self.show_result)
        self.reset_btn = self.builder.get_object('reset_button')
        self.reset_btn.connect('clicked', self.reset)

        adjust = Gtk.Adjustment(value=0, lower=0, upper=9999,
                                step_increment=1, page_increment=0)
        self.suma.configure(adjustment=adjust, climb_rate=1, digits=0)


    def sum_characters(self, btn=None):

        global texto1
        global texto2
        global largo
        texto1 = self.text1.get_text()
        texto2 = self.text2.get_text()
        largo = len(texto1) + len(texto2)
        self.suma.set_value(largo)

    def show_result(self, btn=None):

        self.sum_characters()
        aux = result_dialog()
        response = aux.dialog_window.run()

        if response == Gtk.ResponseType.OK:
            self.text1.set_text('')
            self.text2.set_text('')
            self.suma.set_value(0)

    def reset(self, btn=None):

        aux2 = warning_window()
        response = aux2.reset_window.run()

        if response == Gtk.ResponseType.OK:
            self.text1.set_text('')
            self.text2.set_text('')
            self.suma.set_value(0)

class result_dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia1.glade')

        self.dialog_window = self.builder.get_object('message_window')
        self.dialog_window.set_markup('Sus datos son los siguientes:')
        self.dialog_window.format_secondary_text('Texto 1: %s \n'
                                                 'Texto 2: %s \n'
                                                 'Suma de ambos: %d'
                                                 %(texto1, texto2, largo))
        self.dialog_window.show_all()

        self.accept_btn = self.builder.get_object('info_ok')
        self.accept_btn.connect('clicked', self.btn_ok_press)
        self.info_close = self.builder.get_object('info_close')
        self.info_close.connect('clicked', self.btn_close_press)

    def btn_ok_press(self, btn=None):
        self.dialog_window.destroy()

    def btn_close_press(self, btn=None):
        self.dialog_window.destroy()

class warning_window():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia1.glade')

        self.reset_window = self.builder.get_object('reset_warning')
        self.reset_window.set_markup('¿Seguro que quiere resetear los datos?')

        self.reset_ok = self.builder.get_object('warning_accept')
        self.reset_ok.connect('clicked', self.reset_ok_press)
        self.reset_cancel = self.builder.get_object('warning_cancel')
        self.reset_cancel.connect('clicked', self.reset_cancel_pressed)

    def reset_ok_press(self, btn=None):
        self.reset_window.destroy()

    def reset_cancel_pressed(self, btn=None):
        self.reset_window.destroy()

if __name__ == '__main__':

    main_window()
    Gtk.main()

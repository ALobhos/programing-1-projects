import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import datetime


#Funcion para verificar correo
def invalid_mail(mail):

    aux = False
    counter = 0
    for i in range(len(mail)):

        #Se revisa que haya mas una @ en el correo
        if mail[i] == '@':
            counter += 1

            #Si existen dos o mas @ el correo se considera no valido
            if counter>1:
                aux = True
                break

        #Los espacios tampoco son un caracter valido en un correo
        elif mail[i] == ' ':
            aux = True
            break

    domain_len = 0
    for i in range(len(mail)):

        #Se revisa el largo total del dominio, para posteriormente
        #evaluar si este es valido
        if mail[len(mail)-(1+i)] != '.':
            domain_len += 1

        else:
            break

    #Si el dominio es de un solo caracter o ninguno, se considera no valido
    if domain_len < 2:
        aux = True

    else:
        #Se toma solo la parte del dominio y se compara con una lista de
        #dominios conocidos
        domain = mail[-domain_len:]
        valid_domains = ['org', 'com', 'cl', 'net', 'gov', 'es']

        if domain not in valid_domains:
            aux = True

    return aux


#Funcion para calcular digito verificador de rut
def verificador(rut):

    rut_to_work = []
    backward_aux = len(rut)-3

    #Solo se toman los digitos que van antes del guion del rut entregado
    for i in range(len(rut)-2):

        try:
            #Al rut se le quitan los puntos y se le ordena de atras
            #hacia adelante
            int(rut[backward_aux - i])
            rut_to_work.append(int(rut[backward_aux - i]))

        except ValueError:
            pass

    #Se crea la secuencia de numeros por los que se debe multiplicar
    sequence = [2,3,4,5,6,7,2,3]
    total = 0

    #Cada numero del rut, se multiplica por un numero de la secuencia anterior
    for i in range(len(rut_to_work)):
        total += (rut_to_work[i] * sequence[i])

    #El digito es igual a 11 menos el modulo de 11 aplicado al total anterior
    digit = 11 - (total%11)

    #Si el resultado es 11, se retorna 0; si es 10, k;
    #si no, el numero obtenido
    if digit == 11:
        return '0'

    elif digit == 10:
        return 'k'

    else:
        return str(digit)


#Funcion para validar rut
def invalid_rut(rut):

    #Un rut con puntos y guiones no deberia tener menos de 11 caracteres
    aux = False
    if len(rut) < 11:
        aux = True

    else:

        #Se verifica la existencia de un guion en la posicion especifica
        if rut[len(rut)-2] != '-':
            aux = True

        #Se verifican los puntos que debe contener
        elif rut[len(rut)-6] != '.' or rut[len(rut)-10] != '.':
            aux = True

        #Verificados los dos puntos anteriores, se verifica el digito
        #verificador, es importante este orden para no hacer fallar la
        #funcion que calcula el digito verificador
        elif rut[len(rut)-1] != verificador(rut):
            aux = True

    return aux


#Funcion que valida los datos ingresados
def check_entries(name, residence, rut, mail):

    error = None

    #Un nombre en blanco no es valido
    if name == '':
        error = 'nombre'

    #Una residencia tampoco
    elif residence == '':
        error = 'direccion'

    #El rut se verifica por separado en 2 funciones mas arriba
    elif invalid_rut(rut):
        error = 'rut'

    #El correo tambien se verifica por una funcion aparte
    elif invalid_mail(mail):
        error = 'correo electrónico'

    return error


#Funcion que obtiene la fecha del gtk.calendar
def get_from_calendar(calendar):

    #Se obtiene la fecha y se guarda de forma resumida en un arreglo
    date = calendar.get_date()
    resumed_date = []

    for i in date:
        resumed_date.append(i)

    #Se suma uno al mes para compensar el desfase que entrega la funcion
    #get_date, la cual va del 0 al 11 en los meses
    resumed_date[1] = resumed_date[1]+1

    return resumed_date


#Funcion que obtiene la fecha actual
def get_actual_date():

    #Con ayuda del modulo datetime, se obtiene la fecha actual del equipo
    aux = datetime.datetime.now()
    keys = ['%Y', '%m', '%d']
    date = []

    #La funcion datetime.now() entrega todos los datos juntos y entrega
    #muchos datos (segundos, minutos, etc.), por lo que se almacenan solo
    #los datos utiles en un arreglo con el comando strftime, que obtiene
    #solo un valor especifico, las configuraciones son los % de mas arriba
    for i in keys:
        date.append(int(aux.strftime(i)))

    return date


#Clase para ventana principal
class main_window():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia3.glade')

        main_window = self.builder.get_object('main_window')
        main_window.set_default_size(600, 400)
        main_window.connect('destroy', Gtk.main_quit)
        main_window.set_title('Guia 3: Verificación')
        main_window.show_all()

        #La entrada de nombre tiene la señal changed, que activara el entry
        #de la residencia si hay al menos un caracter en el nombre
        self.name_entry = self.builder.get_object('name_entry')
        self.name_entry.set_placeholder_text('Nombre')
        self.name_entry.connect('changed', self.set_residence_entry)

        #Residencia tiene por defecto su sensibilidad desactivada a menos
        #que exista un caracter en la entrada de nombre
        self.residence_entry = self.builder.get_object('residence_entry')
        self.residence_entry.set_placeholder_text('Residencia')
        self.residence_entry.set_sensitive(False)

        #El calendario calculara la diferencia de fechas cada vez que se
        #seleccione un dia en el
        self.date_entry = self.builder.get_object('date_entry')
        self.date_entry.connect('day-selected', self.calculate_age)
        self.date_difference = self.builder.get_object('date_difference')
        self.rut_entry = self.builder.get_object('rut_entry')
        self.rut_entry.set_placeholder_text('xx.xxx.xxx-y')
        self.mail_entry = self.builder.get_object('mail_entry')
        self.mail_entry.set_placeholder_text('mail@domain.com')

        #Se crea un boton que permita verificar los datos
        self.ok_button = self.builder.get_object('ok_button')
        self.ok_button.connect('clicked', self.check_entry)

    #Metodo para verificar datos
    def check_entry(self, btn=None):

        #Se obtienen todos los textos ingresados en los entry
        name = self.name_entry.get_text()
        residence = self.residence_entry.get_text()
        rut = self.rut_entry.get_text()
        mail = self.mail_entry.get_text()

        #Se llama a la funcion check_entries, que retornara algun error, o
        #una variable vacia en caso de no existir error
        error = check_entries(name, residence, rut, mail)
        #Se abre una ventana de texto a la que se le envia la variable de
        #error, que puede ser nada o tener algun valor
        message_box(error)


    #Metodo que activa el entry de residencia
    def set_residence_entry(self, btn=None):

        #Si existe al menos un caracter en el entry de nombre, la entrada
        #de residencia se activa
        if len(self.name_entry.get_text()) > 0:
            self.residence_entry.set_sensitive(True)

        else:
            self.residence_entry.set_sensitive(False)


    #Metodo para calcular diferencia de fechas
    def calculate_age(self, btn=None):

        #Se obtienen ambas fechas, la actual y la seleccionada
        date = get_from_calendar(self.date_entry)
        actual = get_actual_date()

        #Se calcula la diferencia de años obtenidos
        difference = actual[0] - date[0]

        #Si la fecha de meses y dias selecionados en el calendario aun no
        #pasan, se le resta un año a la diferencia de fechas
        if (actual[2] - date[2]) <= 0:
            if ((actual[1]-1) - date[1]) <= 0:
                difference -= 1

        self.date_difference.set_text('%d años' %(difference))


#Clase para ventana de mensaje
class message_box():

    def __init__(self, error):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('guia3.glade')

        self.dialog = self.builder.get_object('message_dialog')
        self.dialog.show_all()

        #Boton para cerrar cuadro de dialogo
        self.close_btn = self.builder.get_object('close_message')
        self.close_btn.connect('clicked', self.close_dialog)

        #Si el la variable error es None, se entiende que los datos son
        #correctos y se muestra el mensaje correspondiente
        if error is None:
            self.dialog.set_title('Correcto')
            self.dialog.format_secondary_text('Todos los datos han sido'
                                              ' ingresados de forma correcta')

        #En caso de existir uno o mas errores, la ventana de texto dira que
        #algun dato no es valido y se pedira ingresarlo de forma valida
        else:
            self.dialog.set_title('ERROR')
            self.dialog.format_secondary_text('Por favor, ingrese un(a) %s'
                                              ' valido(a)' %(error))

    #Metodo que cierra la ventana de texto
    def close_dialog(self, btn=None):
        self.dialog.destroy()


#Funcion main 
if __name__ == '__main__':

    main_window()
    Gtk.main()

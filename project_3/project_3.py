import gi
import json
import checker
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def get_from_combo(combo):

    combo_aux = combo.get_active_iter()
    if combo_aux is not None:
        model = combo.get_model()
        name = model[combo_aux][0]
    else:
        name = None

    return name


def open_data(to_open):

    try:
        with open('%s.json'%(to_open)) as file:
            data = json.load(file)
        file.close()

    except IOError:
        data = []

    return data


def save_data(data, save):

    with open('%s.json' %(save), 'w') as file:
        json.dump(data, file, indent=4)
    file.close()


class start_window():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        main_window = self.builder.get_object('main_window')
        main_window.set_default_size(900, 600)
        main_window.connect('destroy', Gtk.main_quit)

        self.btn_task = self.builder.get_object('new_task')
        self.btn_task.connect('clicked', self.add_task)
        self.btn_class = self.builder.get_object('new_class')
        self.btn_class.connect('clicked', self.add_class)
        self.btn_partner = self.builder.get_object('new_partner')
        self.btn_partner.connect('clicked', self.add_partner)
        self.btn_edit = self.builder.get_object('edit')
        self.btn_edit.connect('clicked', self.edit_data)

        main_window.show_all()


    def add_task(self, btn=None):
        task_dialog()

    def add_class(self, btn=None):
        class_dialog()

    def add_partner(self, btn=None):
        partner_dialog()

    def edit_data(self, btn=None):
        edit_dialog()

################################################################################
class task_dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        self.task_window = self.builder.get_object('task_window')
        self.task_window.show_all()

        self.task_ok = self.builder.get_object('task_ok')
        self.task_ok.connect('clicked', self.task_ok_pressed)
        self.task_cancel = self.builder.get_object('task_cancel')
        self.task_cancel.connect('clicked', self.task_cancel_pressed)

        self.task_name = self.builder.get_object('task_name')
        self.task_day = self.builder.get_object('task_day')
        self.task_day.connect('changed', self.replace_day)
        self.task_month = self.builder.get_object('task_month')
        self.task_relevance = self.builder.get_object('task_relevance')

    def replace_day(self, btn=None):

        numbers = []
        for i in range(40):
            numbers.append(str(i+1))

        if self.task_day.get_text() in numbers:
            if self.task_day.get_text() != '':
                actual = int(self.task_day.get_text())
                if actual>31:
                    self.task_day.set_text('31')
        else:
            self.task_day.set_text('')


    def task_ok_pressed(self, btn=None):

        tname = self.task_name.get_text()
        tday = self.task_day.get_text()
        tmonth = get_from_combo(self.task_month)
        trelevance = get_from_combo(self.task_relevance)

        if checker.valid_task(tname, tday, tmonth, trelevance):
            data = open_data('tasks')

            data.append({'name': tname,
                         'day': tday,
                         'month': tmonth,
                         'relevance': trelevance})

            save_data(data, 'tasks')
            self.task_window.destroy()

        else:
            task_label = self.builder.get_object('task_label')
            task_label.set_text('Valores ingresados no validos')

    def task_cancel_pressed(self, btn=None):
        self.task_window.destroy()
################################################################################

class class_dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        self.class_window = self.builder.get_object('class_window')
        self.class_window.show_all()

        self.class_ok = self.builder.get_object('class_ok')
        self.class_ok.connect('clicked', self.class_ok_pressed)
        self.class_cancel = self.builder.get_object('class_cancel')
        self.class_cancel.connect('clicked', self.class_cancel_pressed)

        self.class_name = self.builder.get_object('class_name')
        self.class_hours = self.builder.get_object('class_hours')

    def class_ok_pressed(self, btn=None):

        cname = self.class_name.get_text()
        chours = get_from_combo(self.class_hours)
        data = open_data('classes')

        if checker.valid_class(cname):

            data[cname] = []
            adder = hour_adder(data, cname)
            counter = 0

            while counter < (int(chours)):
                signal = adder.hour_dialog.run()

                if signal == Gtk.ResponseType.OK:
                    valid = adder.add_hour()
                    if valid:
                        counter+=1

                else:
                    adder.hour_dialog.destroy()
                    self.class_window.destroy()

            save_data(data, 'classes')
            adder.hour_dialog.destroy()
            self.class_window.destroy()

        else:
            self.class_label = self.builder.get_object('class_label')
            self.class_label.set_text('Nombre no valido o repetido')

    def class_cancel_pressed(self, btn=None):
        self.class_window.destroy()


class hour_adder():


    def __init__(self, data, name):

        self.data = data
        self.name = name
        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        self.hour_dialog = self.builder.get_object('class_adder')
        self.hour_dialog.show_all()

        self.hour_day = self.builder.get_object('class_day')
        self.hour_block = self.builder.get_object('class_block')

        self.add_ok = self.builder.get_object('adder_ok')
        self.add_cancel = self.builder.get_object('adder_cancel')
        self.adder_label = self.builder.get_object('adder_label')


    def add_hour(self, btn=None):

        hday = get_from_combo(self.hour_day)
        hblock = get_from_combo(self.hour_block)

        if checker.valid_block(hday, hblock):
            self.data[self.name].append({hday:hblock})
            self.adder_label.set_text('Añadido exitosamente')
            return True

        else:
            self.adder_label.set_text('Ya existe un ramo en este horario')
            return False


    def adder_cancel_press(self, btn=None):
        self.hour_dialog.destroy()

################################################################################
class partner_dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        self.partner_window = self.builder.get_object('partner_window')
        self.partner_window.show_all()

        self.partner_ok = self.builder.get_object('partner_ok')
        self.partner_ok.connect('clicked', self.partner_ok_pressed)
        self.partner_cancel = self.builder.get_object('partner_cancel')
        self.partner_cancel.connect('clicked', self.partner_cancel_pressed)

        self.mate_name = self.builder.get_object('mate_name')
        self.mate_lastname = self.builder.get_object('mate_lastname')
        self.mate_career = self.builder.get_object('mate_career')
        self.mate_phone = self.builder.get_object('mate_phone')
        self.mate_mail = self.builder.get_object('mate_mail')

    def partner_ok_pressed(self, btn=None):

        partner_name = self.mate_name.get_text()
        partner_lastname = self.mate_lastname.get_text()
        partner_career = self.mate_career.get_text()
        partner_phone = self.mate_phone.get_text()
        partner_mail = self.mate_mail.get_text()

        if checker.valid_partner(partner_name, partner_lastname,
                                 partner_career, partner_phone, partner_mail):

            data = open_data('partners')
            data.append({'name':partner_name,
                         'lastname': partner_lastname,
                         'career': partner_career,
                         'phone': partner_phone,
                         'mail': partner_mail})

            save_data(data, 'partners')
            self.partner_window.destroy()

        else:
            self.partner_label = self.builder.get_object('partner_label')
            self.partner_label.set_text('Datos ingresados no validos')

    def partner_cancel_pressed(self, btn=None):
        self.partner_window.destroy()

class edit_dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        self.edit_window = self.builder.get_object('edit_window')
        self.edit_window.show_all()

        self.edit_task = self.builder.get_object('edit_task')
        self.edit_task.connect('clicked', self.edit_task_pressed)
        self.edit_class = self.builder.get_object('edit_class')
        self.edit_class.connect('clicked', self.edit_class_pressed)
        self.edit_partner = self.builder.get_object('edit_partner')
        self.edit_partner.connect('clicked', self.edit_partner_pressed)
        self.edit_cancel = self.builder.get_object('edit_cancel')
        self.edit_cancel.connect('clicked', self.edit_cancel_pressed)
        self.option = self.builder.get_object('edit_option')


    def edit_task_pressed(self, btn=None):

        data = open_data('tasks')

        if len(data) == 0:
            self.edit_label = self.builder.get_object('edit_label')
            self.edit_label.set_text('No existen trabajos para borrar/editar')

        else:
            edit_option = get_from_combo(self.option)

            if edit_option == 'Borrar':
                destroy_dialog('tasks')
            else:
                a = 1

            self.edit_window.destroy()

    def edit_class_pressed(self, btn=None):

        data = open_data('classes')

        if len(data) == 0:
            self.edit_label = self.builder.get_object('edit_label')
            self.edit_label.set_text('No existen ramos para borrar/editar')

        else:
            edit_option = get_from_combo(self.option)

            if edit_option == 'Borrar':
                destroy_dialog('classes')
            else:
                a = 1
            self.edit_window.destroy()

    def edit_partner_pressed(self, btn=None):

        data = open_data('partners')

        if len(data) == 0:
            self.edit_label = self.builder.get_object('edit_label')
            self.edit_label.set_text('No existen compañeros para borrar/editar')

        else:
            edit_option = get_from_combo(self.option)

            if edit_option == 'Borrar':
                destroy_dialog('partners')
            else:
                a = 1
            self.edit_window.destroy()

    def edit_cancel_pressed(self, btn=None):
        self.edit_window.destroy()
################################################################################

class destroy_dialog():

    def __init__(self, destroy):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('project_3.glade')

        self.destroy_window = self.builder.get_object('destroy_dialog')
        self.destroy_window.show_all()

        self.label = self.builder.get_object('destroy_label')
        self.label.set_text('¿Que %s desea eliminar?' %(destroy))

        self.destroy_ok = self.builder.get_object('destroy_ok')
        #self.destroy_ok.con

        delete = Gtk.ListStore(str)
        data = open_data(destroy)

        if destroy == 'classes':
            for name in data:
                delete.append([name])

        else:
            for group in data:
                print(group['name'])
                delete.append([group['name']])

        self.delete_combo = self.builder.get_object('destroy_combo')
        self.delete_combo.set_model(delete)
        cell_renderer = Gtk.CellRendererText()
        self.delete_combo.pack_start(cell_renderer, True)
        self.delete_combo.add_attribute(cell_renderer, 'text', 0)



if __name__ == '__main__':

    start = start_window()
    Gtk.main()
